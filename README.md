# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository allows you to access the custom comparator jar
* Version 1.0
* This jar currently works on Strings for sorting purposes, it has not been tested for integer values

### How do I get set up? ###

* You do not have to do anything , just download this jar and add it to your projects under libs folder and add it as a dependency

### Code snippet

The calling function would look like:


```
#!Java

public static List<Grades> getSortedList(List<Grades> gradeList, Class<?> classType, String methodName) {
		Collections.sort(gradeList, new CustomComparator<Grades>(classType, methodName));
		return gradeList;
	}
```


Where classType will refer to the class you wish to sort, in this case it will be Grades.class and methodName will refer to the property on which you wish to perform the sort,  so we send in Grades.getGradeNameMethodName() (as we would be sorting on the grade name)

The Grade class will look like this:


```
#!Java

public class Grades {
    private String gradeId;
    private String gradeName;
	
    public String getGradeId() {
	return gradeId;
    }
    public void setGradeId(String gradeId) {
	this.gradeId = gradeId;
    }
    public String getGradeName() {
	return gradeName;
    }
    public void setGradeName(String gradeName) {
	this.gradeName = gradeName;
    }
	
    // needed for sorting by the custom comparator jar
    public static String getGradeNameMethodName() {
	return "getGradeName";
    }
}
```


## Input:
Grade 1
Grade 10
Grade 2
Grade 5
Grade 3
Grade 4

## Output
Grade 1
Grade 2
Grade 3
Grade 4
Grade 5
Grade 10

### Who do I talk to? ###

* Dhara Shah - sdhara2@hotmail.com